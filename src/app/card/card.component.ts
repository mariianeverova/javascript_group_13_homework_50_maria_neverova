import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() title = 'Avengers Endgame';
  @Input() year = 'April 26, 2019';
  @Input() url = 'https://m.media-amazon.com/images/I/81ai6zx6eXL._AC_SL1304_.jpg';
}
