import {Component} from "@angular/core";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  name = 'Top 3 Marvel Movies';
  url = 'https://cdn.icon-icons.com/icons2/607/PNG/512/film-roll-side-view_icon-icons.com_56303.png';
}
